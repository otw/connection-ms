package com.otw.connectionms;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.otw.connectionms.exception.NodeException;
import com.otw.connectionms.service.NodeService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
public class ConnectionmsComponent implements CommandLineRunner {

    private NodeService connectionService;

    public ConnectionmsComponent(NodeService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void run(String... args) throws Exception {
        //connectionService.deleteAll();
        try {
            System.out.println(connectionService.getUserNode(1L));
        } catch (NodeException e) {
            System.out.println("ogm init");
        }
    }


}
