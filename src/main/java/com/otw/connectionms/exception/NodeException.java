package com.otw.connectionms.exception;

public class NodeException extends Throwable {

    public NodeException(String message) {
        super(message);
    }
}
