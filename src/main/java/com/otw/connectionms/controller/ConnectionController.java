package com.otw.connectionms.controller;

import com.otw.connectionms.dto.ConnectedContactDTO;
import com.otw.connectionms.dto.FileDTO;
import com.otw.connectionms.dto.NodeDTO;
import com.otw.connectionms.exception.NodeException;
import com.otw.connectionms.model.ContactNode;
import com.otw.connectionms.service.NodeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/connection")
public class ConnectionController {
    private final NodeService nodeService;
    private final ModelMapper modelMapper;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${contact.serviceUrl}")
    private String contactUrl;

    @Value("${event.serviceUrl}")
    private String eventUrl;

    @Value("${dossier.dossierUrl}")
    private String dossierUrl;

    public ConnectionController(NodeService nodeService, ModelMapper modelMapper) {
        this.nodeService = nodeService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/node/{id}")
    ResponseEntity<NodeDTO> getNode(@PathVariable Long id) throws NodeException {
        ContactNode contactNodeDb = nodeService.getNode(id);
        return new ResponseEntity<>(modelMapper.map(contactNodeDb, NodeDTO.class), HttpStatus.OK);
    }

    @GetMapping("/userConnectionsWithInfo")
    ResponseEntity<List<ConnectedContactDTO>> getUserNodes(@RequestHeader("User-ID") Long userId) throws NodeException {
        ContactNode userContactNode = nodeService.getUserNode(userId);
        if (userContactNode != null && userContactNode.getUserConnections() != null) {
            String values = nodeService.getConnectedContactNodes(userContactNode);

            if (values.length() > 0) {

                String url = contactUrl + "/contact/connectedUsers?userids=" + values;

                ResponseEntity<List<ConnectedContactDTO>> response = restTemplate.exchange(
                        url,
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<List<ConnectedContactDTO>>() {
                        });
                List<ConnectedContactDTO> contacts = response.getBody();

                return new ResponseEntity<>(contacts, HttpStatus.OK);
            }
        }
        return new ResponseEntity(HttpStatus.OK);

    }

    @PutMapping("/addContactOrEventConnection/{contactId}")
    ResponseEntity addEventConnection(@RequestHeader("User-ID") Long userId, @PathVariable("contactId") Long contactId, @RequestBody FileDTO fileDTO) throws NodeException {

        if (fileDTO.getEventId() != null) {
            nodeService.addEventConnection(userId, fileDTO.getEventId());
            nodeService.addEventConnection(contactId, fileDTO.getEventId());
        }

        nodeService.addContact(userId, contactId);

        String url = dossierUrl + "/dossier/addFileToDossier";
        createFileOfDossier(url, userId, fileDTO, contactId);
        createFileOfDossier(url, contactId, fileDTO, userId);


        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    private void createFileOfDossier(String url, Long userId, FileDTO fileDTO, Long contactId) {

        restTemplate.exchange(
                url,
                HttpMethod.PUT,
                nodeService.createRequest(userId, fileDTO, contactId),
                FileDTO.class);

    }

    @GetMapping("/userEventsWithInfo")
    ResponseEntity<List<Object>> getUserEvents(@RequestHeader("User-ID") Long userId) throws NodeException {
        ContactNode userContactNode = nodeService.getUserNode(userId);
        if (userContactNode != null && userContactNode.getEventConnections() != null) {
            String values = nodeService.getConnectedEventNodes(userContactNode);

            if (values.length() > 0) {

                String url = eventUrl + "/event/connectedEvents?eventids=" + values;

                ResponseEntity<List<Object>> response = restTemplate.exchange(
                        url,
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<List<Object>>() {
                        });
                List<Object> events = response.getBody();

                return new ResponseEntity<>(events, HttpStatus.OK);
            }
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/userConnections")
    ResponseEntity getUserConnections(@RequestHeader("USER-ID") Long userId) throws NodeException {
        ContactNode userContactNode = nodeService.getUserNode(userId);
        if (userContactNode != null && userContactNode.getUserConnections() != null) {
            Object values = nodeService.getConnections(userContactNode);
            return new ResponseEntity(values, HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/addContact/{contactId}")
    ResponseEntity addConnection(@RequestHeader("User-ID") Long userId, @PathVariable("contactId") Long contactId) throws NodeException {
        nodeService.addContact(userId, contactId);

        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @PutMapping("/addEventConnection/{eventId}")
    ResponseEntity addEventConnection(@RequestHeader("User-ID") Long userId, @PathVariable("eventId") Long eventId) throws NodeException {
        nodeService.addEventConnection(userId, eventId);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/deleteConnection/{idToDelete}")
    ResponseEntity deleteConnection(@RequestHeader("User-ID") Long userId, @PathVariable("idToDelete") Long idToDelete) throws NodeException {
        ContactNode userContactNode = nodeService.getUserNode(userId);
        int deleteIndex = -1;
        for (int i = 0; i < userContactNode.getUserConnections().size(); i++) {
            if (userContactNode.getUserConnections().get(i).getContactId() == idToDelete) {
                deleteIndex = i;
            }
        }

        if (deleteIndex != -1) {
            userContactNode.getUserConnections().remove(deleteIndex);
        }

        nodeService.save(userContactNode);

        return new ResponseEntity(HttpStatus.OK);
    }
}
