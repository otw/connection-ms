package com.otw.connectionms.repository;

import com.otw.connectionms.model.EventNode;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EventNodeRepository extends CrudRepository<EventNode, Long> {
    Optional<EventNode> findEventNodeByEventId(Long eventId);
}
