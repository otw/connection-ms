package com.otw.connectionms.repository;

import com.otw.connectionms.model.ContactNode;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ContactNodeRepository extends CrudRepository<ContactNode, Long> {
    List<ContactNode> findAll();

    Optional<ContactNode> findContactNodeByContactId(Long id);

    ContactNode save(ContactNode connection);

    List<ContactNode> save(List<ContactNode> connections);

    @Query(value = "MATCH path = (n:ContactNode {contactId: {idParam}})-[*..1]-(e)\n" +
            "WITH COLLECT(path) as paths\n" +
            "CALL apoc.convert.toTree(paths) yield value as json\n" +
            "RETURN json")
    Collection<?> findNodesByNodeId(@Param("idParam") Long idParam);
}
