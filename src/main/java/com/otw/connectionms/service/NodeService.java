package com.otw.connectionms.service;

import com.otw.connectionms.dto.FileDTO;
import com.otw.connectionms.exception.NodeException;
import com.otw.connectionms.model.ContactNode;
import com.otw.connectionms.model.EventNode;
import com.otw.connectionms.model.NodeType;
import com.otw.connectionms.repository.ContactNodeRepository;
import com.otw.connectionms.repository.EventNodeRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class NodeService {
    private final ContactNodeRepository contactNodeRepository;
    private final EventNodeRepository eventNodeRepository;

    public NodeService(ContactNodeRepository contactNodeRepository, EventNodeRepository eventNodeRepository) {
        this.contactNodeRepository = contactNodeRepository;
        this.eventNodeRepository = eventNodeRepository;
    }

    public List<ContactNode> getNodes() {
        return contactNodeRepository.findAll();
    }

    public void doNothing() {
    }

    public ContactNode getNode(Long id) throws NodeException {
        Optional<ContactNode> node = contactNodeRepository.findById(id);
        if (node.isPresent()) {
            return node.get();
        }

        throw new NodeException("ContactNode not found!");
    }

    public ContactNode getUserNode(Long userId) throws NodeException {
        Optional<ContactNode> node = contactNodeRepository.findContactNodeByContactId(userId);
        if (node.isPresent()) {
            return node.get();
        } else {
            throw new NodeException("ContactNode not found!");
        }
    }

    public EventNode getEventNode(Long eventId) throws NodeException {
        Optional<EventNode> node = eventNodeRepository.findEventNodeByEventId(eventId);
        if (node.isPresent()) {
            return node.get();
        } else {
            throw new NodeException("EventNode not found!");
        }
    }

    public ContactNode getUserNodeWithCreate(Long userId) {
        Optional<ContactNode> node = contactNodeRepository.findContactNodeByContactId(userId);
        return node.orElse(null);
    }

    public EventNode getEventNodeWithCreate(Long eventId) {
        Optional<EventNode> node = eventNodeRepository.findEventNodeByEventId(eventId);
        return node.orElse(null);
    }

    public ContactNode save(ContactNode connection) {
        return contactNodeRepository.save(connection);
    }

    public EventNode saveEventNode(EventNode eventNode) {
        return eventNodeRepository.save(eventNode);
    }

    public List<ContactNode> save(List<ContactNode> connections) {
        return contactNodeRepository.save(connections);
    }

    public void deleteAll() {
        contactNodeRepository.deleteAll();
        eventNodeRepository.deleteAll();
    }

    public void addContact(Long userId, Long connectionId) throws NodeException {
        initContactNodeIfNotPresent(userId);
        initContactNodeIfNotPresent(connectionId);

        ContactNode userContactNode = this.getUserNode(userId);
        ContactNode contactNodeToAdd = this.getUserNode(connectionId);

        addContactToUserNode(userContactNode, contactNodeToAdd);

    }

    public void addEventConnection(Long userId, long eventId) throws NodeException {
        initContactNodeIfNotPresent(userId);
        initEventNodeIfNotPresent(eventId);

        ContactNode userContactNode = this.getUserNode(userId);
        EventNode eventNodeToAdd = this.getEventNode(eventId);

        addEventToUserNode(userContactNode, eventNodeToAdd);

    }

    //TODO check if user id exists
    private void initContactNodeIfNotPresent(Long idToInitialise) throws NodeException {
        if (getUserNodeWithCreate(idToInitialise) == null) {
            this.save(new ContactNode(idToInitialise, NodeType.CONTACT));
        }
    }

    private void initEventNodeIfNotPresent(Long idToInitialise) throws NodeException {
        if (getEventNodeWithCreate(idToInitialise) == null) {
            this.saveEventNode(new EventNode(idToInitialise, NodeType.EVENT));
        }
    }


    private void addContactToUserNode(ContactNode userContactNode, ContactNode contactNodeToAdd) {
        List<ContactNode> userContactNodes = userContactNode.getUserConnections();
        if (userContactNodes != null) {
            if (!userContactNodes.contains(contactNodeToAdd)) {
                userContactNode.addContact(contactNodeToAdd);
                this.save(userContactNode);
            }
        } else {
            userContactNode.addContact(contactNodeToAdd);
            this.save(userContactNode);
        }
    }

    private void addEventToUserNode(ContactNode userContactNode, EventNode eventNodeToAdd) {
        List<EventNode> eventConnections = userContactNode.getEventConnections();
        if (eventConnections != null) {
            if (!eventConnections.contains(eventNodeToAdd)) {
                userContactNode.addEvent(eventNodeToAdd);
                this.save(userContactNode);
            }
        } else {
            userContactNode.addEvent(eventNodeToAdd);
            this.save(userContactNode);
        }
    }

    public String getConnectedContactNodes(ContactNode userContactNode) {
        List<String> connections = userContactNode.getUserConnections().stream()
                .map(n -> n.getContactId().toString()).collect(Collectors.toList());
        return String.join(",", connections);
    }

    public String getConnectedEventNodes(ContactNode userContactNode) {
        List<String> connections = userContactNode.getEventConnections().stream()
                .map(n -> n.getEventId().toString()).collect(Collectors.toList());
        return String.join(",", connections);
    }

    public HttpEntity<FileDTO> createRequest(Long userId, FileDTO fileDTO, Long contactId) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("User-ID", userId.toString());
        fileDTO.setContactId(contactId);
        return new HttpEntity<>(fileDTO, headers);
    }

    public Object getConnections(ContactNode userContactNode) throws NodeException {
        Collection<?> nodes = contactNodeRepository.findNodesByNodeId(userContactNode.getContactId());
        if (nodes.iterator().hasNext()) {
            return nodes.iterator().next();
        }
        throw new NodeException("Could not find node");
    }


}
