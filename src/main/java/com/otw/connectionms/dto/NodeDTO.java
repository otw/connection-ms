package com.otw.connectionms.dto;


import java.util.List;

public class NodeDTO {
    private Long id;
    private Long nodeId;
    private List<NodeDTO> connections;

    public NodeDTO(Long nodeId) {
        this.nodeId = nodeId;
    }

    public NodeDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public List<NodeDTO> getConnections() {
        return connections;
    }

    public void setConnections(List<NodeDTO> connections) {
        this.connections = connections;
    }
}
