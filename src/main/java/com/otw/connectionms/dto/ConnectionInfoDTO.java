package com.otw.connectionms.dto;

public class ConnectionInfoDTO {
    private Long nodeId;
    private String firstName;
    private String lastName;
}
