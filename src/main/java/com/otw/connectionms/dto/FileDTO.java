package com.otw.connectionms.dto;

import java.time.LocalDateTime;

public class FileDTO {

    private Long id;
    private Long contactId;
    private LocalDateTime time;
    private Long latitude;
    private Long longitude;
    private String title;
    private String note;
    private Long eventId;

    public FileDTO(){}

    public FileDTO(Long id, Long contactId, LocalDateTime time, Long latitude, Long longitude, String title, String note, Long eventId) {
        this.id = id;
        this.contactId = contactId;
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
        this.note = note;
        this.eventId = eventId;
    }

    public FileDTO(Long contactId, LocalDateTime time, Long latitude, Long longitude, String title, String note, Long eventId) {
        this.contactId = contactId;
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
        this.title = title;
        this.note = note;
        this.eventId = eventId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Long getLatitude() {
        return latitude;
    }

    public void setLatitude(Long latitude) {
        this.latitude = latitude;
    }

    public Long getLongitude() {
        return longitude;
    }

    public void setLongitude(Long longitude) {
        this.longitude = longitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }
}
