package com.otw.connectionms.model;

public enum NodeType {
    CONTACT, EVENT
}
