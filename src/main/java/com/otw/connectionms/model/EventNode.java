package com.otw.connectionms.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@NodeEntity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "ref")
public class EventNode {
    @GeneratedValue
    @Id
    @JsonBackReference
    private Long id;
    private Long eventId;
    @Relationship(type = "CONNECTED_WITH", direction = Relationship.UNDIRECTED)
    private List<ContactNode> userConnections;
    private NodeType nodeType;

    public EventNode() {
    }

    public EventNode(Long eventId, NodeType nodeType) {
        this.eventId = eventId;
        this.nodeType = nodeType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public List<ContactNode> getUserConnections() {
        return userConnections;
    }

    public void setUserConnections(List<ContactNode> userConnections) {
        this.userConnections = userConnections;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public void setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
    }
}
