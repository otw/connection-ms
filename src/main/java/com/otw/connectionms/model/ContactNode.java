package com.otw.connectionms.model;

import com.fasterxml.jackson.annotation.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.ArrayList;
import java.util.List;


@NodeEntity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "ref")
public class ContactNode {
    @GeneratedValue
    @Id
    @JsonBackReference
    private Long id;
    private Long contactId;
    @Relationship(type = "CONNECTED_WITH", direction = Relationship.UNDIRECTED)
    private List<ContactNode> userConnections;
    @Relationship(type = "ATTENDED", direction = Relationship.UNDIRECTED)
    private List<EventNode> eventConnections;
    private NodeType nodeType;

    public ContactNode(Long contactId, NodeType nodeType) {
        this.contactId = contactId;
        this.nodeType = nodeType;
    }

    public ContactNode() {
    }

    public void addContact(ContactNode newContact) {
        if (this.userConnections == null) {
            this.userConnections = new ArrayList<>();
        }

        this.userConnections.add(newContact);
    }

    public void addEvent(EventNode newEvent) {
        if (this.eventConnections == null) {
            this.eventConnections = new ArrayList<>();
        }

        this.eventConnections.add(newEvent);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public List<ContactNode> getUserConnections() {
        return userConnections;
    }

    public void setUserConnections(List<ContactNode> userConnections) {
        this.userConnections = userConnections;
    }

    public List<EventNode> getEventConnections() {
        return eventConnections;
    }

    public void setEventConnections(List<EventNode> eventConnections) {
        this.eventConnections = eventConnections;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public void setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
    }
}
