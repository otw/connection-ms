package com.otw.connectionms.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.otw.connectionms.dto.FileDTO;
import com.otw.connectionms.exception.NodeException;
import com.otw.connectionms.model.ContactNode;
import com.otw.connectionms.model.NodeType;
import com.otw.connectionms.service.NodeService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ConnectionControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private NodeService nodeService;

    @Before
    public void init() {
    }

    @Test
    public void addConnection() throws Exception, NodeException {

        FileDTO fileDTO = new FileDTO(2L, null, 0L, 0L, "KdG Stagemomentje", "Standard note", 2L);

        mockMvc.perform(put("/connection/addContactOrEventConnection/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(fileDTO))
                .header("User-ID", 1))
                .andExpect(status().isAccepted())
                .andReturn();

        ContactNode contactNode = nodeService.getUserNode(1L);
        Assert.assertTrue(contactNode.getUserConnections().size() > 0);

    }

}
